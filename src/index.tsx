import React from "react";
import ReactDOM from "react-dom";
import Routing from "./routing";
import "./index.scss";

ReactDOM.render(
  <React.StrictMode>
    <Routing />
  </React.StrictMode>,
  document.getElementById("root")
);
