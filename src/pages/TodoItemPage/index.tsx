import React, { useEffect } from "react";
import { todosPageStore } from "../../store";
import { observer } from "mobx-react-lite";
import CenterContainer from "../../components/CenterContainer";
import "./style.scss";

function TodoItemPage() {
  useEffect(() => {
    todosPageStore.getTodo();
  }, []);
  return todosPageStore.selectedTodo !== null ? (
    <CenterContainer>
      <div className="todo-info">
        <span className="todo-info-status">
          {todosPageStore.selectedTodo.completed
            ? "Задача выполнена"
            : "Задача не выполнена"}
        </span>
        <span className="todo-info-title">
          {todosPageStore.selectedTodo.title}
        </span>
        <div className="todo-info-actions">
          <button className="todo-info-complete-btn">Выполнить</button>
          <button className="todo-info-edit-btn">Редактировать</button>
          <button className="todo-info-delete-btn">Удалить</button>
        </div>
      </div>
    </CenterContainer>
  ) : null;
}

export default observer(TodoItemPage);
