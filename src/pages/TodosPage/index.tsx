import React, { useEffect, FormEvent } from "react";
import TodoList from "../../containers/TodoList";
import CenterContainer from "../../components/CenterContainer";
import { todosPageStore } from "../../store";
import { observer } from "mobx-react-lite";
import "./style.scss";

function TodosPage() {
  useEffect(() => {
    todosPageStore.getAllTodos();
  }, []);

  function onEnter(e: FormEvent) {
    e.preventDefault();
    todosPageStore.addTodo(todosPageStore.inputTaskTitleValue);
    todosPageStore.setInputTaskTitleValue("");
  }

  return (
    <CenterContainer>
      <div className="adding-todo">
        <form onSubmit={onEnter}>
          <input
            type="text"
            placeholder="Задача"
            value={todosPageStore.inputTaskTitleValue}
            onChange={(e) =>
              todosPageStore.setInputTaskTitleValue(e.target.value)
            }
          />
          <button type="submit" disabled={!todosPageStore.inputTaskTitleValue}>
            Создать
          </button>
        </form>
      </div>
      {/* <TodoList /> */}
    </CenterContainer>
  );
}

export default observer(TodosPage);
