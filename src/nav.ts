export function index() {
  return "/";
}

export function todo(todoId: string) {
  return `/todo/${todoId}`;
}
