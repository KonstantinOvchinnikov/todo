import { ITodo } from "../../types/todo";
import axios from "axios";
const API_URL = "https://jsonplaceholder.typicode.com/todos";

export const todosApi = {
  getAllTodos() {
    return axios.get(API_URL);
  },

  getTodo(todoId: number) {
    return axios.get(`${API_URL}/${todoId}`);
  },

  addTodo(todo: ITodo) {
    return axios.post(API_URL, todo);
  },

  deleteTodo(todoId: number) {
    return axios.delete(`${API_URL}/${todoId}`);
  },
};
