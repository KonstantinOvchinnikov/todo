import { FC } from "react";

export interface IRoute {
  title: string;
  exact: boolean;
  path: string;
  component: FC;
}
