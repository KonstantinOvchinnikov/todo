import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";
import TodosPage from "./pages/TodosPage";
import TodoItemPage from "./pages/TodoItemPage";
import { IRoute } from "./types";
import * as nav from "./nav";

export default function Routing() {
  const routes: IRoute[] = [
    {
      title: "Home page",
      exact: true,
      path: nav.index(),
      component: TodosPage,
    },
    {
      title: "Todo page",
      exact: true,
      path: nav.todo(":id"),
      component: TodoItemPage,
    },
  ];
  return (
    <Router>
      <Switch>
        {routes.map((route) => (
          <Route
            key={route.title}
            exact={route.exact}
            path={route.path}
            component={route.component}
          />
        ))}
        <Redirect to={nav.index()} />
      </Switch>
    </Router>
  );
}
