import React, { MouseEvent } from "react";
import RemoveIcon from "../../assets/images/delete.png";
import "./style.scss";
type Props = {
  title: string;
  isCompleted: boolean;
  handleChangeCompleted: () => void;
  remove: () => void;
};

export default function TodoCard(props: Props) {
  function handleRemove(e: MouseEvent) {
    e.stopPropagation();
    props.remove();
  }

  return (
    <div className="todo-card">
      <span className="todo-card-title">{props.title}</span>
      <div className="todo-card-actions">
        <input
          onClick={(e) => e.stopPropagation()}
          type="checkbox"
          checked={props.isCompleted}
          onChange={props.handleChangeCompleted}
        />
        <img
          src={RemoveIcon}
          alt="remove icon"
          width={20}
          height={20}
          onClick={(e: MouseEvent) => handleRemove(e)}
        />
      </div>
    </div>
  );
}
