import TodoCard from "../../components/TodoCard";
import { ITodo } from "../../types/todo";
import { observer } from "mobx-react-lite";
import { useHistory } from "react-router-dom";
import * as nav from "../../nav";

type Props = {
  setSelectedTodo: (todo: ITodo) => void;
  getTodo: () => void;
  deleteTodo: (todoId: number) => void;
  handleChangeCompleted: (todo: ITodo) => void;
  todos: ITodo[];
};

function TodoList(props: Props) {
  const navigate = useHistory();

  function openDetailedTodoInfo(todo: ITodo) {
    props.setSelectedTodo(todo);
    navigate.push(nav.todo(String(todo.id)));
    props.getTodo();
  }

  return (
    <div>
      {props.todos.length > 0 ? (
        props.todos.map((todo: ITodo) => (
          <div key={todo.id} onClick={() => openDetailedTodoInfo(todo)}>
            <TodoCard
              isCompleted={todo.completed}
              title={todo.title}
              handleChangeCompleted={() => props.handleChangeCompleted(todo)}
              remove={() => props.deleteTodo(todo.id)}
            />
          </div>
        ))
      ) : (
        <div>Список задач пуст!</div>
      )}
    </div>
  );
}

export default observer(TodoList);
