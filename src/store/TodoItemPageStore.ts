import { ITodo } from "../types/todo";
import { makeAutoObservable } from "mobx";

class TodoPageStore {
  selectedTodo: ITodo | null = null;

  constructor() {
    makeAutoObservable(this);
  }

  setSelectedTodo(todo: ITodo) {
    this.selectedTodo = todo;
  }
}

export const todoPageStore = new TodoPageStore();
