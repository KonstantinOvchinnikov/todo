import { todosApi } from "../provider/api/todos";
import { ITodo } from "../types/todo";
import { makeAutoObservable } from "mobx";

class TodosPageStore {
  todos: ITodo[] = [];
  inputTaskTitleValue: string = "";
  selectedTodo: ITodo | null = null;

  constructor() {
    makeAutoObservable(this);
  }
  handleChangeCompleted(todo: ITodo) {
    todo.completed = !todo.completed;
  }

  setInputTaskTitleValue(value: string) {
    this.inputTaskTitleValue = value;
  }

  setTodos(todos: ITodo[]) {
    this.todos = todos;
  }
  setSelectedTodo(todo: ITodo) {
    this.selectedTodo = todo;
  }
  async addTodo(newTodoTitle: string) {
    try {
      const newTodo: ITodo = {
        userId: 1,
        id: Date.now(),
        title: newTodoTitle,
        completed: false,
      };
      await todosApi.addTodo(newTodo);
      console.log("Задача создана");
    } catch (e) {
      console.log(e);
    }
  }

  async deleteTodo(todoId: number) {
    try {
      await todosApi.deleteTodo(todoId);
    } catch (e) {
      console.log(e);
    }
  }

  async getTodo() {
    try {
      if (this.selectedTodo !== null) {
        const result = await todosApi.getTodo(this.selectedTodo.id);
        this.setSelectedTodo(result.data);
        console.log(result.data);
      }
    } catch (e) {
      console.log(e);
    }
  }
  async getAllTodos() {
    try {
      const result = await todosApi.getAllTodos();
      this.setTodos(result.data);
    } catch (e) {
      console.log(e);
    }
  }

  removeTodo(removableTodoId: number) {
    this.todos = this.todos.filter((todo) => todo.id !== removableTodoId);
  }
}

export const todosPageStore = new TodosPageStore();
